Glosario: 
	
-Request :El comando HTTP Request permite enviar todo tipo de petición HTTP a un URL específico y procesar la respuesta del servidor HTTP.
-Response : indican si se ha completado satisfactoriamente una solicitud HTTP específica
=Status Codes: Indica codigos de respuesta de http los cuales son de 5 tipos
				1xx = Respuestas informativas
				2xx = Peticion correcta
				3xx = Redirecciones dentro de la pagina
				4xx = Errores del cliente
				5xx = Errores del servidor
-Methods : 	GET : Se usa para recuperar la informacion del servidor dado ,dada una URI, Get 				solo recupera data y no deberia tener efecto en otra data

		 	POST : Se usa para enviar informacion al servidor

		 	HEAD : lo mismo que get con la diferencia que solo devuelve la linea de status y la seccion del header respectivamente

			OPTIONS : Describe las opciones de comunicacion para un recurso objetivo

			PUT : Reemplaza todas las representaciones del recurso objetivo con el contenido subido

			DELETE : Elimina todas las representaciones del recurso objetivo dado un URI

-Header: son la parte central de los HTTP requests y responses, y transmiten información acerca del navegador del cliente, de la página solicitada, del servidor, etc.


Headers:
	-Accept: Se utiliza para especificar que tipo de media es aceptable para el response

	-Accept-Charset:Se utiliza para especificar que tipo de caracteres son aceptables para el response

	-Accept-Encoding: Es similar al accept, pero restringe las codificaciones del contenido

	-Cache-Control: Se utiliza para especificar directivas que deben ser obedecidas por todo el sistema de almacenamiento en caché

	-Connection : Controla si la conexion se mantiene abierta o no luego de que alguna accion/tranferencia ocurra 

	-Cookie : Valor que contiene un par nombre/valor de informacion almacenada para una URL

	-Set-Cookie : Valor que contiene un par nombre/valor de informacion conservado para esta URL

	-Host : Especifica el anfitrion(host) de internet y el numero de puerto para el recurso pedido

	-Origin : Indica de dónde se origina una búsqueda

	-Referer : permite al cliente especificar la direccion (URI) del recurso de la url que ha sido pedida

	-User-Agent : contiene informacion sobre el usuario de donde se origino el request

	-Content-Encoding : Es usado para modificar el tipo de media
	 
	-Content-Length : Indica el tamannio de el entity-body
	  
	-Content-Type : Indica el tipo de media que el entity-body envia al recipente o al metodo HEAD

	-Location : Es usado para redireccionar el recipiente a una locacion que no sea la request-URI

	-Upgrade : Habilita a que el cliente que protocolos adicionales de comunicacion soporta y que quisiera
	usar si el servidor encuentra apropiado cambiar protocolos



Tabla de REQ/RES http://www.inf.ucv.cl/

| REQ/ RES | Metodo HTTP  (solo REQ) |                                                                   URL                                                                  |                                                                               Headers (solo Nombres)                                                                              | Status (solo RES) |                                  Descripcion                                  |
|:--------:|:-----------------------:|:--------------------------------------------------------------------------------------------------------------------------------------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:-----------------:|:-----------------------------------------------------------------------------:|
|    REQ   |           GET           |                                                           http://inf.ucv.cl/                                                           |                                                                   Upgrade-Insecure-Requests: User-Agent: Accept:                                                                  |        n/a        |                           Peticion inicial del sitio                          |
|    RES   |           n/a           |                                                                   n/a                                                                  |                Redirect to: Date: Server: X-Powered-By: Expires: Cache-Control: Pragma: Location: Vary: Content-Encoding: Content-Length: Connection: Content-Type:               |        200        | Se devuelve correctamente el sitio. Al parecer no se encontró en algún caché. |
|    REQ   |           GET           |                                             http://fonts.googleapis.com/css?family=Voltaire                                            |                                                                                User-Agent: Accept:                                                                                |        n/a        |       Peticion para obtener la font voltaire con los parametros del link      |
|    RES   |           n/a           |                                                                   n/a                                                                  |                                                                  Redirect to: Location: Non-Authoritative-Reason:                                                                 |        307        |                          Se devuelve el font deseado                          |
|    REQ   |           GET           |                                     http://www.inf.ucv.cl/wp-content/themes/folder/css/widgets.css                                     |                                                                                User-Agent: Accept:                                                                                |        n/a        |             Se pide el archivo css para  el estilo de los widgets             |
|    RES   |           n/a           |                                                                   n/a                                                                  |           Date: Server: Accept-Ranges: Pragma: X-Powered-By: Etag: Expires: Cache-Control: X-Content-Type-Options: Vary: Content-Encoding: Content-Length: Content-Type:          |        200        |        Se devuelve con exito el archivo de estilon css para los widgets       |
|    REQ   |           GET           |                                   http://www.inf.ucv.cl/wp-content/uploads/2014/09/logo_escuela1.png                                   |                                                                                User-Agent: Accept:                                                                                |        n/a        |         Se pide una imagen para mostrar (En este caso el logo escuela)        |
|    RES   |           n/a           |                                                                   n/a                                                                  |     Date: Server: Accept-Ranges: Pragma: X-Powered-By: Etag: Expires: Cache-Control: X-Content-Type-Options: Vary: Content-Encoding: Content-Length: Connection: Content-Type:    |        200        |                     Se devuelve con exito la imagen pedida                    |
|    REQ   |           GET           | http://www.inf.ucv.cl/wp-content/themes/folder/php/timthumb.php?src=wp-content/uploads/2014/09/algoritmosdeoptimizacion.jpg&h=73&w=188 |                                                                                User-Agent: Accept:                                                                                |        n/a        |        Se pide una imagen de las que se van transicionando en el tiempo       |
|    RES   |           n/a           |                                                                   n/a                                                                  | Date: Server: X-Powered-By: Accept-Ranges: Last-Modified: Cache-Control: Vary: Expires: Etag: X-Content-Type-Options: Content-Encoding: Content-Length: Connection: Content-Type: |        200        |            Se devuelve correctamente la imagen que va transcionando           |


Tabla de REQ/RES https://www.facebook.com/

| REQ/ RES | Metodo HTTP  (solo REQ) |                                                               URL                                                               |                                                                                                                                                                     Headers (solo Nombres)                                                                                                                                                                    | Status (solo RES) |                                  Descripcion                                 |
|:--------:|:-----------------------:|:-------------------------------------------------------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:-----------------:|:----------------------------------------------------------------------------:|
|    REQ   |           GET           |                                                    https://www.facebook.com/                                                    |                                                                                                                                                         Upgrade-Insecure-Requests: User-Agent: Accept:                                                                                                                                                        |        n/a        |                          Peticion inicial del sitio                          |
|    RES   |           n/a           |                                                               n/a                                                               |                                                               status: cache-control: expires: vary: Expires: x-content-type-options: pragma: strict-transport-security: content-encoding: content-security-policy: x-frame-options: expect-ct: x-xss-protection: content-type: x-fb-debug: date:                                                              |        200        | Se devuelve correctamente el sitio. Al parecer no se encontró en algun cache |
|    REQ   |           GET           |                                   https://static.xx.fbcdn.net/rsrc.php/v3/yl/r/mkgQoXIMIAM.png                                  |                                                                                                                                                                      User-Agent: Accept:                                                                                                                                                                      |        n/a        |        Peticion para obtener iconos que utiliza facebook para grupos         |
|    RES   |           n/a           |                                                               n/a                                                               |                                                                                     status: content-type: last-modified: x-content-type-options: timing-allow-origin: access-control-allow-origin: content-md5: cache-control: expires: x-fb-debug: content-length: date:                                                                                     |        200        |                       Se devuelve los iconos  deseados                       |
|    REQ   |           GET           | https://video.fscl11-1.fna.fbcdn.net/hvideo-frc3-atn/v/rg83twBE6xKUhk3xWEilq/live-dash/live-md-a/2640881726138226_0-2448835.m4a |                                                                                                                                                                  Origin: User-Agent: Accept:                                                                                                                                                                  |        n/a        |                    Se pide el hud del reproductor de video                   |
|    RES   |           n/a           |                                                               n/a                                                               | status: server: content-type: last-modified: etag: accept-ranges: x-fb-config-version-olb-prod: timing-allow-origin: access-control-allow-origin: x-fb-origin-hit: cache-control: access-control-expose-headers: target-clusters: to-edge-cps: x-fb-config-version-elb-prod: x-fb-edge-hit: content-length: x-fb-config-version-flb-prod: date: x-fb-fna-hit: |        200        |             Se devuelve con exito el hud del reproductor de video            |
|    REQ   |           GET           |                               https://static.xx.fbcdn.net/rsrc.php/v3/yS/l/0,cross/Kxf3JMppHVu.css                              |                                                                                                                                                                  Origin: User-Agent: Accept:                                                                                                                                                                  |        n/a        |    Se pide un archivo css para aplicar un estilo alguna clase de la pagina   |
|    RES   |           n/a           |                                                               n/a                                                               |                                                                         status: content-type: last-modified: vary: x-content-type-options: content-encoding: timing-allow-origin: access-control-allow-origin: cache-control: expires: content-md5: x-fb-debug: content-length: date:                                                                         |        200        |                      Se acepta el archivo css con exito                      |
|    REQ   |           GET           |                                   https://static.xx.fbcdn.net/rsrc.php/v3/yE/r/fx7ZES_ni-0.gif                                  |                                                                                                                                                                      User-Agent: Accept:                                                                                                                                                                      |        n/a        |    Se pide un gif que es referente a la animacion cuando se carga un video   |
|    RES   |           n/a           |                                                               n/a                                                               |                                                                                     status: content-type: last-modified: x-content-type-options: timing-allow-origin: access-control-allow-origin: content-md5: cache-control: expires: x-fb-debug: content-length: date:                                                                                     |        200        |                          Se devuelve el archivo gif                          |